import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MainTests
{

    public static <T> List<T> semmetricDifference(
            List<T> a,
            List<T> b
    )
    {

        List<T> aa = new ArrayList<>(a);
        aa.retainAll(b);
        List<T> result = new ArrayList<>(a);
        result.addAll(b);
        result.remove(aa);

        return result;

    }

    public static void main(String[] args)
    {

        List<Integer> a = new ArrayList<>();

        a.add(1);
        a.add(2);
        a.add(4);

        List<Integer> b = new ArrayList<>();

        b.add(10);
        b.add(5);
        b.add(4);

        List<Integer> result = semmetricDifference(a,b);

        result.forEach(p -> System.out.println(p));

    }

}
